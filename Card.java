public class Card{
	private Suit Suits;
	private Value Value;
	
	public Card(Suit suit, Value value){
		this.Suits = suit;
		this.Value = value;
	}
	
	public Suit getSuit(){
		return this.Suits;
	}
	
	public Value getValue(){
		return this.Value;
	}
	
	public String toString(){
		return this.Value + " of " + this.Suits;
	}
	
	public double calculateScore(){
		return this.Suits.getScore() + this.Value.getScore();
	}
}