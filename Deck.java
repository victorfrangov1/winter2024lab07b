import java.util.Random;
public class Deck {
  private Card[] cards;
  private int numberOfCards;
  private Random rng;

  public Deck() {
    Suit[] suits = Suit.values();
    Value[] value = Value.values();
    this.numberOfCards = 52;
    this.rng = new Random();
    this.cards = new Card[52];

	int i = 0;
    for (Suit k : suits) {
      for (Value j : value) {
        this.cards[i] = new Card(k, j);
        i++;
      }
    }
  }

  public int length() {
    return this.numberOfCards;
  }

  public Card drawTopCard() {
    this.numberOfCards--;
	Card result = this.cards[this.numberOfCards];
    return result;
  }

  public String toString() {
	String result = "";
    for (int i = 0; i < numberOfCards; i++) {
      result += this.cards[i] + "\n";
    }
    return result;
  }

  public void shuffle() {
    for (int i = 0; i < this.numberOfCards - 1; i++) {
		int pos = this.rng.nextInt(this.numberOfCards - 1);
		Card temp = this.cards[i];
		this.cards[i] = this.cards[pos];
		this.cards[pos] = temp;
    }
  }
}
