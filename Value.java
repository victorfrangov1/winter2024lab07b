public enum Value {
	  A(1),
	  Two(2),
	  Three(3),
	  Four(4),
	  Five(5),
	  Six(6),
	  Seven(7),
	  Eight(8),
	  Nine(9),
	  Ten(10),
	  J(11),
	  Q(12),
	  K(13);
	  
	private double score;
  
	private Value(double score){
		this.score = score;
	}
	
	public double getScore() {
        return this.score;
    }
  }