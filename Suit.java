public enum Suit{
	Hearts(0.4),
	Spades(0.3),
	Clubs(0.1),
	Diamonds(0.2);
  
	private double score;
  
	private Suit(double score){
		this.score = score;
	}
	
	public double getScore() {
        return this.score;
    }
}